﻿using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using NLog;
using Library.Core.Entities;
using Library.Core.ResponseModels;
using Library.Data.Repositories;
using Library.View.ViewModels;

namespace Library.Services
{
    public class ArticleServices
    {
        private readonly ArticleRepository _articlesRepository;
        private readonly AuthorRepository _authorsRepository;
        private readonly JournalInArticlesRepository _journalInArticlesRepository;
        private readonly AuthorInArticlesRepository _authorInArticlesRepository;
        private readonly JournalRepository _journalsRepository;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public ArticleServices(string connectionString)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            _journalsRepository = new JournalRepository(connection);
            _authorsRepository = new AuthorRepository(connection);
            _articlesRepository = new ArticleRepository(connection);
            _journalInArticlesRepository = new JournalInArticlesRepository(connection);
            _authorInArticlesRepository = new AuthorInArticlesRepository(connection);
        }

        public ArticleViewModel Create(ArticleViewModel newArticleViewModel)
        {
            try
            {
                var article = CreateArticleFromArticleViewModel(newArticleViewModel);
                _articlesRepository.Insert(article);
                var articleViewModel = new ArticleViewModel(newArticleViewModel);

                if (articleViewModel.JournalsIds.Any())
                {
                    InsertJournalsOfArticle(article.Id, articleViewModel.JournalsIds);
                    articleViewModel.JournalsNames = _journalsRepository
                                                 .SelectJournalsNamesByArticleId(article.Id)
                                                 .ToList();
                }
                if (articleViewModel.AuthorsIds.Any())
                {
                    InsertArticlesOfAuthor(article.Id, articleViewModel.AuthorsIds);
                    articleViewModel.AuthorsNames = _authorsRepository
                                                    .SelectAuthorsNamesByArticleId(article.Id)
                                                    .ToList();
                }
                return articleViewModel;
            }
            catch
            {
                _logger.Error("Error in Create method.");
                throw;
            }
        }

        public ArticleViewModel Update(ArticleViewModel newArticleViewModel)
        {
            try
            {
                Article article = _articlesRepository.Select(newArticleViewModel.Id);
                FillArticleFromArticleViewModel(ref article, newArticleViewModel);
                _articlesRepository.Update(article);
                UpdateJournalsOfArticle(newArticleViewModel.JournalsIds, article.Id);
                UpdateAuthorsOfArticle(article.Id, newArticleViewModel.AuthorsIds);

                var updatedArticleViewModel = new ArticleViewModel(newArticleViewModel);
                if (updatedArticleViewModel.JournalsIds.Any())
                {
                    updatedArticleViewModel.JournalsNames = _journalsRepository
                                                 .SelectJournalsNamesByArticleId(article.Id)
                                                 .ToList();
                }
                if (updatedArticleViewModel.AuthorsIds.Any())
                {
                    updatedArticleViewModel.AuthorsNames = _authorsRepository
                                                    .SelectAuthorsNamesByArticleId(article.Id)
                                                    .ToList();
                }
                return updatedArticleViewModel;
            }
            catch
            {
                _logger.Error("Error in Update method.");
                throw;
            }
        }

        public void Delete(string id)
        {
            try
            {
                _journalInArticlesRepository.DeleteArticleWithJournalsRelations(id);
                _authorInArticlesRepository.DeleteArticleWithAuthorsRelations(id);
                _articlesRepository.Delete(id);
            }
            catch
            {
                _logger.Error("Error in Delete method.");
                throw;
            }
        }

        public IEnumerable<ArticleViewModel> GetAll()
        {
            try
            {
                IEnumerable<ArticleResponseModel> articleJournalsAuthorsResponse = _articlesRepository
                                                                               .SelectArticlesWithJournals();
                List<ArticleViewModel> articlesViewModels = articleJournalsAuthorsResponse
                                                          .GroupBy(articleJournalsAuthors => articleJournalsAuthors.ArticleId)
                                                          .Select(grouping => new ArticleViewModel(grouping))
                                                          .ToList();
                return articlesViewModels;
            }
            catch
            {
                _logger.Error("Error in GetAll method.");
                throw;
            }
        }

        public IEnumerable<ArticleViewModel> GetArticlesInJournals()
        {
            try
            {
                List<Article> articles = _articlesRepository
                                         .SelectAll().ToList();
                List<ArticleViewModel> articlesViewModels = articles
                                                            .Select(article => new ArticleViewModel(article))
                                                            .ToList();
                return articlesViewModels;
            }
            catch
            {
                _logger.Error("Error in GetArticlesInJournals method.");
                throw;
            }
        }

        public Article CreateArticleFromArticleViewModel(ArticleViewModel articleViewModel)
        {
            var article = new Article();
            article.Name = articleViewModel.Name;
            article.Date = articleViewModel.Date;
            return article;
        }

        public void FillArticleFromArticleViewModel(ref Article article, ArticleViewModel articleViewModel)
        {
            article.Name = articleViewModel.Name;
            article.Date = articleViewModel.Date;
        }

        public void InsertJournalsOfArticle(string articleId, List<string> journalIds)
        {
            var JournalInArticles = new List<JournalInArticles>();
            foreach (string item in journalIds)
            {
                if (string.IsNullOrEmpty(item))
                {
                    continue;
                }
                var journalArticle = new JournalInArticles();
                journalArticle.ArticleId = articleId;
                journalArticle.JournalId = item;
                JournalInArticles.Add(journalArticle);
            }
            _journalInArticlesRepository.Insert(JournalInArticles);
        }

        public void InsertArticlesOfAuthor(string articleId, List<string> authorsIds)
        {
            var AuthorInArticles = new List<AuthorInArticles>();
            foreach (string item in authorsIds)
            {
                if (string.IsNullOrEmpty(item))
                {
                    continue;
                }
                AuthorInArticles authorArticle = new AuthorInArticles();
                authorArticle.ArticleId = articleId;
                authorArticle.AuthorId = item;
                AuthorInArticles.Add(authorArticle);
            }
            _authorInArticlesRepository.Insert(AuthorInArticles);
        }

        public void UpdateJournalsOfArticle(List<string> articleNewJournalsIds, string articleId)
        {
            List<string> articleOldJournalsIds = _journalsRepository
                                             .SelectJournalsIdsByArticleId(articleId)
                                             .ToList();
            List<string> idsToInsert = articleNewJournalsIds
                                       .Except(articleOldJournalsIds)
                                       .ToList();
            List<string> idsToDelete = articleOldJournalsIds
                                       .Except(articleNewJournalsIds)
                                       .ToList();
            if (idsToDelete.Any())
            {
                _journalInArticlesRepository.DeleteArticleWithJournalsRelations(articleId, idsToDelete);
            }
            if (idsToInsert.Any())
            {
                InsertJournalsOfArticle(articleId, idsToInsert);
            }
        }

        public void UpdateAuthorsOfArticle(string articleId, List<string> articleNewAuthors)
        {
            List<string> articleOldAuthorIds = _authorsRepository
                                                .SelectAuthorsIdsByArticleId(articleId)
                                                .ToList();
            List<string> idsToInsert = articleNewAuthors
                                       .Except(articleOldAuthorIds)
                                       .ToList();
            List<string> idsToDelete = articleOldAuthorIds
                                       .Except(articleNewAuthors)
                                       .ToList();
            if (idsToDelete.Any())
            {
                _authorInArticlesRepository.DeleteArticleWithAuthorsRelations(articleId, idsToDelete);
            }
            if (idsToInsert.Any())
            {
                InsertArticlesOfAuthor(articleId, idsToInsert);
            }
        }
    }
}
