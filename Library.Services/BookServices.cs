﻿using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NLog;
using Library.Core.Entities;
using Library.Core.ResponseModels;
using Library.Data.Repositories;
using Library.View.ViewModels;

namespace Library.Services
{
    public class BookServices
    {
        private readonly BookRepository _booksRepository;
        private readonly AuthorRepository _authorsRepository;
        private readonly AuthorInBooksRepository _authorInBooksRepository;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public BookServices(string connectionString)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            _booksRepository = new BookRepository(connection);
            _authorsRepository = new AuthorRepository(connection);
            _authorInBooksRepository = new AuthorInBooksRepository(connection);
        }

        public BookViewModel Create(BookViewModel newBookViewModel)
        {
            try
            {
                Book newBook = CreateBookFromBookViewModel(newBookViewModel);
                _booksRepository.Insert(newBook);
                var bookViewModel = new BookViewModel(newBookViewModel);
                if (newBookViewModel.AuthorsIds.Any())
                {
                    InsertAuthorsOfBook(newBook.Id, newBookViewModel.AuthorsIds);
                    bookViewModel.AuthorsNames = _authorsRepository
                                                 .SelectAuthorsNamesByBookId(newBook.Id)
                                                 .ToList();
                }
                return bookViewModel;
            }
            catch
            {
                _logger.Error("Error in Create method.");
                throw;
            }
        }

        public BookViewModel Update(BookViewModel newBookViewModel)
        {
            try
            {
                Book book = _booksRepository.Select(newBookViewModel.Id);
                FillBookFromBookViewModel(ref book, newBookViewModel);
                _booksRepository.Update(book);
                UpdateAuthorsOfBook(book.Id, newBookViewModel.AuthorsIds);
                var updatedBookViewModel = new BookViewModel(newBookViewModel);
                if (newBookViewModel.AuthorsIds.Any())
                {
                    updatedBookViewModel.AuthorsNames = _authorsRepository
                                                        .SelectAuthorsNamesByBookId(book.Id)
                                                        .ToList();
                }
                return updatedBookViewModel;
            }
            catch
            {
                _logger.Error("Error in Update method.");
                throw;
            }
        }

        public void Delete(string id)
        {
            try
            {
                _authorInBooksRepository.DeleteBookWithAuthorsRelations(id);
                _booksRepository.Delete(id);
            }
            catch
            {
                _logger.Error("Error in Delete method.");
                throw;
            }
        }

        public List<BookViewModel> GetAll()
        {
            try
            {
                List<BookResponseModel> booksWithAuthors = _booksRepository
                                                           .SelectBooksWithAuthors()
                                                           .ToList();
                List<BookViewModel> views = booksWithAuthors
                                            .GroupBy(AuthorInBooks => AuthorInBooks.BookId)
                                            .Select(grouping => new BookViewModel(grouping))
                                            .ToList();
                return views;
            }
            catch
            {
                _logger.Error("Error in GetAll method.");
                throw;
            }
        }

        public List<BookViewModel> GetBooksInAuthors()
        {
            try
            {
                List<Book> books = _booksRepository
                                   .SelectAll()
                                   .ToList();
                List<BookViewModel> bookViewModels = books
                                    .Select(book => new BookViewModel(book))
                                    .ToList();
                return bookViewModels;
            }
            catch
            {
                _logger.Error("Error in GetBooksInAuthors method.");
                throw;
            }
        }

        public Book CreateBookFromBookViewModel(BookViewModel bookViewModel)
        {
            var newBook = new Book();
            newBook.Name = bookViewModel.Name;
            newBook.CountOfPages = bookViewModel.CountOfPages;
            newBook.Year = bookViewModel.Year;
            newBook.Price = bookViewModel.Price;
            newBook.Genre = bookViewModel.Genre;
            newBook.Content = bookViewModel.Content;
            return newBook;
        }

        public void FillBookFromBookViewModel(ref Book book, BookViewModel bookViewModel)
        {
            book.Name = bookViewModel.Name;
            book.CountOfPages = bookViewModel.CountOfPages;
            book.Year = bookViewModel.Year;
            book.Price = bookViewModel.Price;
            book.Genre = bookViewModel.Genre;
            book.Content = bookViewModel.Content;
        }

        public void InsertAuthorsOfBook(string bookId, List<string> authorsIds)
        {
            var AuthorInBooks = new List<AuthorInBooks>();
            foreach (string item in authorsIds)
            {
                if (string.IsNullOrEmpty(item))
                {
                    continue;
                }
                var authorBook = new AuthorInBooks();
                authorBook.BookId = bookId;
                authorBook.AuthorId = item;
                AuthorInBooks.Add(authorBook);
            }
            _authorInBooksRepository.Insert(AuthorInBooks);
        }

        public void UpdateAuthorsOfBook(string bookId, List<string> newAuthorsIds)
        {
            List<string> OldAuthorsIds = _authorsRepository
                                          .SelectAuthorsIdsByBookId(bookId)
                                          .ToList();
            List<string> idsToInsert = newAuthorsIds
                                       .Except(OldAuthorsIds)
                                       .ToList();
            List<string> idsToDelete = OldAuthorsIds
                                       .Except(newAuthorsIds)
                                       .ToList();
            if (idsToDelete.Any())
            {
                _authorInBooksRepository.DeleteBookWithAuthorsRelations(bookId, idsToDelete);
            }
            if (idsToInsert.Any())
            {
                InsertAuthorsOfBook(bookId, idsToInsert);
            }
        }
    }
}
