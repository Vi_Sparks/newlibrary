﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NLog;
using Library.Core.Entities;
using Library.Core.ResponseModels;
using Library.Data.Repositories;
using Library.View.ViewModels;
namespace Library.Services
{
    public class JournalServices
    {
        private readonly JournalRepository _journalsRepository;
        private readonly ArticleRepository _articlesRepository;
        private readonly AuthorRepository _authorsRepository;
        private readonly JournalInArticlesRepository _journalInArticlesRepository;
        private readonly AuthorInArticlesRepository _authorInArticlesRepository;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public JournalServices(string connectionString)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            _journalsRepository = new JournalRepository(connection);
            _articlesRepository = new ArticleRepository(connection);
            _journalInArticlesRepository = new JournalInArticlesRepository(connection);
            _authorInArticlesRepository = new AuthorInArticlesRepository(connection);
            _authorsRepository = new AuthorRepository(connection);
        }

        public JournalViewModel Create(JournalViewModel journalViewModel)
        {
            try
            {
                var newJournal = CreateJournalFromJournalViewModel(journalViewModel);
                _journalsRepository.Insert(newJournal);
                var updatedJournalViewModel = new JournalViewModel(newJournal);
                if (journalViewModel.ArticlesIds.Any())
                {
                    InsertArticlesOfJournal(newJournal.Id, journalViewModel.ArticlesIds);
                    updatedJournalViewModel.ArticlesIds = _articlesRepository
                                                          .SelectArticlesIdsByJournalId(newJournal.Id)
                                                          .ToList();
                    updatedJournalViewModel.ArticlesNames = _articlesRepository
                                                            .SelectArticlesNamesByJournalId(newJournal.Id)
                                                            .ToList();
                }
                return updatedJournalViewModel;
            }
            catch
            {
                _logger.Error("Error in Create method.");
                return null;
            }
        }

        public JournalViewModel Update(JournalViewModel newJournalViewModel)
        {
            try
            {
                Journal journal = _journalsRepository.Select(newJournalViewModel.Id);
                FillJournalFromJournalViewModel(ref journal, newJournalViewModel);
                var updatedJournalViewModel = new JournalViewModel(journal);
                _journalsRepository.Update(journal);
                UpdateArticlesOfJournal(journal.Id, newJournalViewModel.ArticlesIds);
                if (newJournalViewModel.ArticlesIds.Any())
                {
                    updatedJournalViewModel.ArticlesNames = _articlesRepository
                                                            .SelectArticlesNamesByJournalId(updatedJournalViewModel.Id)
                                                            .ToList();
                    updatedJournalViewModel.ArticlesIds = _articlesRepository
                                                          .SelectArticlesIdsByJournalId(updatedJournalViewModel.Id)
                                                          .ToList();
                }
                
                return updatedJournalViewModel;
            }
            catch
            {
                _logger.Error("Error in Update method.");
                throw;
            }
        }

        public void Delete(string id)
        {
            try
            {
                _journalInArticlesRepository.DeleteJournalWithArticlesRelations(id);
                _journalsRepository.Delete(id);
            }
            catch
            {
                _logger.Error("Error in Delete method.");
                throw;
            }
        }

        public IEnumerable<JournalViewModel> GetAllJournals()
        {
            try
            {
                IEnumerable<JournalResponseModel> journalsWithArticles = _journalsRepository
                                                             .SelectJournalsWithArticles()
                                                             .ToList();
                List<JournalViewModel> journalsViewModels = journalsWithArticles
                                                .GroupBy(JournalInArticles => JournalInArticles.JournalId)
                                                .Select(grouping => new JournalViewModel(grouping))
                                                .ToList();
                return journalsViewModels;
            }
            catch
            {
                _logger.Error("Error in GetAll method.");
                throw;
            }
        }

        public IEnumerable<JournalViewModel> GetJournalsInArticles()
        {
            try
            {
                List<Journal> journals = _journalsRepository
                                         .SelectAll().ToList();
                List<JournalViewModel> journalsViewModels = journals
                                                            .Select(journal => new JournalViewModel(journal))
                                                            .ToList();
                return journalsViewModels;
            }
            catch
            {
                _logger.Error("Error in GetJournalsInArticles method.");
                throw;
            }
        }

        public Journal CreateJournalFromJournalViewModel(JournalViewModel bookViewModel)
        {
            var newJournal = new Journal();
            newJournal.Name = bookViewModel.Name;
            newJournal.CountOfPages = bookViewModel.CountOfPages;
            newJournal.Date = bookViewModel.Date;
            newJournal.Price = bookViewModel.Price;
            newJournal.Content = bookViewModel.Content;
            return newJournal;
        }

        public void FillJournalFromJournalViewModel(ref Journal journal, JournalViewModel journalViewModel)
        {
            journal.Name = journalViewModel.Name;
            journal.CountOfPages = journalViewModel.CountOfPages;
            journal.Date = journalViewModel.Date;
            journal.Price = journalViewModel.Price;
            journal.Content = journalViewModel.Content;
        }

        public void InsertArticlesOfJournal(string journalId, List<string> articlesIds)
        {
            var JournalInArticles = new List<JournalInArticles>();
            foreach (string item in articlesIds)
            {
                if (string.IsNullOrEmpty(item))
                {
                    continue;
                }
                var journalArticle = new JournalInArticles();
                journalArticle.JournalId = journalId;
                journalArticle.ArticleId = item;
                JournalInArticles.Add(journalArticle);
            }
            _journalInArticlesRepository.Insert(JournalInArticles);
        }

        public void UpdateArticlesOfJournal(string journalId, List<string> newArticlesIds)
        {
            List<string> oldArticlesIds = _articlesRepository
                                          .SelectArticlesIdsByJournalId(journalId)
                                          .ToList();
            List<string> idsToInsert = newArticlesIds
                                       .Except(oldArticlesIds)
                                       .ToList();
            List<string> idsToDelete = oldArticlesIds
                                       .Except(newArticlesIds)
                                       .ToList();
            if (idsToDelete.Any())
            {
                _journalInArticlesRepository.DeleteJournalWithArticlesRelations(journalId, idsToDelete);
            }
            if (idsToInsert.Any())
            {
                InsertArticlesOfJournal(journalId, idsToInsert);
            }
        }
    }
}

