﻿using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NLog;
using Library.Core.Entities;
using Library.Core.ResponseModels;
using Library.Data.Repositories;
using Library.View.ViewModels;

namespace Library.Services
{
    public class AuthorServices
    {
        private readonly AuthorRepository _authorsRepository;
        private readonly AuthorInArticlesRepository _AuthorInArticlesRepository;
        private readonly AuthorInBooksRepository _authorInBooksRepository;
        private readonly ArticleRepository _articlesRepository;
        private readonly BookRepository _booksRepository;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public AuthorServices(string connectionString)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            _authorsRepository = new AuthorRepository(connection);
            _AuthorInArticlesRepository = new AuthorInArticlesRepository(connection);
            _authorInBooksRepository = new AuthorInBooksRepository(connection);
            _articlesRepository = new ArticleRepository(connection);
            _booksRepository = new BookRepository(connection);
        }

        public AuthorViewModel Create(AuthorViewModel newAuthorViewModel)
        {
            try
            {
                Author author = CreateAuthorFromAuhtorViewModel(newAuthorViewModel);
                _authorsRepository.Insert(author);
                var authorViewModel = new AuthorViewModel(newAuthorViewModel);
                if (authorViewModel.BooksIds.Any())
                {
                    InsertBooksOfAuthor(authorViewModel.BooksIds, author.Id);
                    authorViewModel.BooksNames = _booksRepository
                                                 .SelectBooksNames(author.Id)
                                                 .ToList();
                }
                if (authorViewModel.ArticlesIds.Any())
                {
                    InsertArticlesOfAuthor(authorViewModel.ArticlesIds, author.Id);
                    authorViewModel.ArticlesNames = _articlesRepository
                                                    .SelectArticlesNamesByAuthorId(author.Id)
                                                    .ToList();
                }
                return authorViewModel;
            }
            catch
            {
                _logger.Error("Error in Create method.");
                throw;
            }
        }

        public AuthorViewModel Update(AuthorViewModel newAuthorViewModel)
        {
            try
            {
                Author author = _authorsRepository.SelectById(newAuthorViewModel.Id);
                FillAuthorFromAuthorViewModel(ref author, newAuthorViewModel);
                _authorsRepository.Update(author);
                UpdateBooksOfAuthor(newAuthorViewModel.BooksIds, author.Id);
                UpdateArticlesOfAuthor(newAuthorViewModel.ArticlesIds, author.Id);
                var updatedAuthorViewModel = new AuthorViewModel(newAuthorViewModel);
                if (updatedAuthorViewModel.BooksIds.Any())
                {
                    updatedAuthorViewModel.BooksNames = _booksRepository
                                                 .SelectBooksNames(newAuthorViewModel.Id)
                                                 .ToList();
                }
                if (updatedAuthorViewModel.ArticlesIds.Any())
                {
                    updatedAuthorViewModel.ArticlesNames = _articlesRepository
                                                    .SelectArticlesNamesByAuthorId(newAuthorViewModel.Id)
                                                    .ToList();
                }
                return updatedAuthorViewModel;
            }
            catch
            {
                _logger.Error("Error in Update method.");
                throw;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                _authorInBooksRepository.DeleteAuthorWithBooksRelations(id);
                _AuthorInArticlesRepository.DeleteAuthorWithArticlesRelations(id);
                _authorsRepository.Delete(id);
                return true;
            }
            catch
            {
                _logger.Error("Error in Delete method.");
                throw;
            }
        }

        public IEnumerable<AuthorViewModel> GetAll()
        {
            try
            {
                IEnumerable<AuthorResponseModel> authorInBooksArticlesResponse = _authorsRepository
                                                                               .SelectAuthorsWithBooksAndArticles();
                List<AuthorViewModel> authorsViewModels = authorInBooksArticlesResponse
                                                          .GroupBy(AuthorInBooksArticles => AuthorInBooksArticles.AuthorId)
                                                          .Select(grouping => new AuthorViewModel(grouping))
                                                          .ToList();
                return authorsViewModels;
            }
            catch
            {
                _logger.Error("Error in GetAll method.");
                throw;
            }
        }

        public IEnumerable<AuthorViewModel> GetAuthorsInBooks()
        {
            try
            {
                IEnumerable<Author> authors = _authorsRepository
                                              .SelectAll();
                List<AuthorViewModel> authorsViewModels = authors
                                                          .Select(author => new AuthorViewModel(author))
                                                          .ToList();
                return authorsViewModels;
            }
            catch
            {
                _logger.Error("Error in GetAuthorsInBooks method.");
                throw;
            }

        }

        public Author CreateAuthorFromAuhtorViewModel(AuthorViewModel authorViewModel)
        {
            var author = new Author();
            author.Name = authorViewModel.Name;
            return author;
        }

        public void FillAuthorFromAuthorViewModel(ref Author author, AuthorViewModel authorViewModel)
        {
            author.Name = authorViewModel.Name;
        }

        public void InsertBooksOfAuthor(List<string> booksIds, string authorId)
        {
            var AuthorInBooks = new List<AuthorInBooks>();
            foreach (string item in booksIds)
            {
                if (string.IsNullOrEmpty(item))
                {
                    continue;
                }
                var authorBook = new AuthorInBooks();
                authorBook.AuthorId = authorId;
                authorBook.BookId = item;
                AuthorInBooks.Add(authorBook);
            }
            _authorInBooksRepository.Insert(AuthorInBooks);
        }

        public void InsertArticlesOfAuthor(List<string> articlesIds, string authorId)
        {
            var AuthorInArticles = new List<AuthorInArticles>();
            foreach (string item in articlesIds)
            {
                if (string.IsNullOrEmpty(item))
                {
                    continue;
                }
                var authorArticle = new AuthorInArticles();
                authorArticle.AuthorId = authorId;
                authorArticle.ArticleId = item;
                AuthorInArticles.Add(authorArticle);
            }
            _AuthorInArticlesRepository.Insert(AuthorInArticles);
        }

        public void UpdateBooksOfAuthor(List<string> authorNewBooksIds, string authorId)
        {
            List<string> authorOldBooksIds = _booksRepository
                                             .SelectBooksIds(authorId)
                                             .ToList();
            List<string> idsToInsert = authorNewBooksIds
                                       .Except(authorOldBooksIds)
                                       .ToList();
            List<string> idsToDelete = authorOldBooksIds
                                       .Except(authorNewBooksIds)
                                       .ToList();
            if (idsToDelete.Any())
            {
                _authorInBooksRepository.DeleteAuthorWithBooksRelations(authorId, idsToDelete);
            }
            if (idsToInsert.Any())
            {
                InsertBooksOfAuthor(idsToInsert, authorId);
            }
        }

        public void UpdateArticlesOfAuthor(List<string> authorNewArticlesIds, string authorId)
        {
            List<string> authorOldArticlesIds = _articlesRepository
                                                .SelectArticlesIdsByAuthorId(authorId)
                                                .ToList();
            List<string> idsToInsert = authorNewArticlesIds
                                       .Except(authorOldArticlesIds)
                                       .ToList();
            List<string> idsToDelete = authorOldArticlesIds
                                       .Except(authorNewArticlesIds)
                                       .ToList();
            if (idsToDelete.Any())
            {
                _AuthorInArticlesRepository.DeleteAuthorWithArticlesRelations(authorId, idsToDelete);
            }
            if (idsToInsert.Any())
            {
                InsertArticlesOfAuthor(idsToInsert, authorId);
            }
        }
    }
}
