﻿using Library.Core.Entities;
using Library.Core.ResponseModels;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace Library.View.ViewModels
{
    public class BookViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int CountOfPages { get; set; }
        public int Year { get; set; }
        public float Price { get; set; }
        public string Genre { get; set; }
        public string Content { get; set; }
        public List<string> AuthorsIds { get; set; }
        public List<string> AuthorsNames { get; set; }

        public BookViewModel()
        {
            AuthorsIds = new List<string>();
            AuthorsNames = new List<string>();
        }

        public BookViewModel(Book book)
        {
            Id = book.Id;
            Name = book.Name;
            Year = book.Year;
            Price = book.Price;
            Genre = book.Genre;
            Content = book.Content;
            AuthorsIds = new List<string>();
            AuthorsNames = new List<string>();
        }

        public BookViewModel(JObject book)
        {
            bool valueExists = true;
            valueExists = book.TryGetValue("Id", out JToken value);
            Id = book.TryGetValue("Id", out value) ? book["Id"].Value<string>() : "";
            Name = book.TryGetValue("Name", out value) ? book["Name"].Value<string>() : "";
            Year = book.TryGetValue("Year", out value) ? book["Year"].Value<int>() : 0;
            Price = book.TryGetValue("Price", out value) ? book["Price"].Value<float>() : 0f;
            Genre = book.TryGetValue("Genre", out value) ? book["Genre"].Value<string>() : "";
            Content = book.TryGetValue("Content", out value) ? book["Content"].Value<string>() : "";
            JArray authorsIds = book.TryGetValue("AuthorsIds", out value) ? book["AuthorsIds"].Value<JArray>() : null;
            AuthorsIds = authorsIds?.ToObject<List<string>>();
        }

        public BookViewModel(BookViewModel bookViewModel)
        {
            Id = bookViewModel.Id;
            Name = bookViewModel.Name;
            CountOfPages = bookViewModel.CountOfPages;
            Year = bookViewModel.Year;
            Price = bookViewModel.Price;
            Genre = bookViewModel.Genre;
            Content = bookViewModel.Content;
            AuthorsIds = bookViewModel.AuthorsIds;
        }

        public BookViewModel(IGrouping<string, BookResponseModel> bookResponseModels)
        {
            Id = bookResponseModels.Key;
            Name = bookResponseModels.First().BookName;
            CountOfPages = bookResponseModels.First().CountOfPages;
            Year = bookResponseModels.First().Year;
            Price = bookResponseModels.First().Price;
            Genre = bookResponseModels.First().Genre;
            Content = bookResponseModels.First().Content;
            AuthorsIds = new List<string>();
            AuthorsNames = new List<string>();
            foreach (var item in bookResponseModels)
            {
                if (AuthorsIds.Contains(item.AuthorId) || item.AuthorId == null)
                {
                    continue;
                }
                AuthorsIds.Add(item.AuthorId);
                AuthorsNames.Add(item.AuthorName);
            }
        }
    }
}
