﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Library.Core.Entities;
using Library.Core.ResponseModels;

namespace Library.View.ViewModels
{
    public class ArticleViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public List<string> JournalsIds { get; set; }
        public List<string> JournalsNames { get; set; }
        public List<string> AuthorsIds { get; set; }
        public List<string> AuthorsNames { get; set; }

        public ArticleViewModel()
        {
            List<string> JournalsIds = new List<string>();
            List<string> Journals = new List<string>();
            List<string> AuthorsIds = new List<string>();
            List<string> AuthorsNames = new List<string>();
        }

        public ArticleViewModel(Article article)
        {
            Id = article.Id;
            Name = article.Name;
            Date = article.Date;
        }

        public ArticleViewModel(JObject article)
        {
            bool valueExists = true;
            valueExists = article.TryGetValue("Id", out JToken value);
            Id = article.TryGetValue("Id", out value) ? article["Id"].Value<string>() : "";
            Name = article.TryGetValue("Name", out value) ? article["Name"].Value<string>() : "";
            Date = article.TryGetValue("Date", out value) ? article["Date"].Value<DateTime>() : DateTime.Now;
            JArray authorsIds = article.TryGetValue("AuthorsIds", out value) ? article["AuthorsIds"].Value<JArray>() : null;
            AuthorsIds = authorsIds?.ToObject<List<string>>();
            JArray journalsIds = article.TryGetValue("JournalsIds", out value) ? article["JournalsIds"].Value<JArray>() : null;
            JournalsIds = journalsIds?.ToObject<List<string>>();
        }

        public ArticleViewModel(ArticleViewModel articleViewModel)
        {
            Id = articleViewModel.Id;
            Name = articleViewModel.Name;
            Date = articleViewModel.Date;
            JournalsIds = articleViewModel.JournalsIds;
            AuthorsIds = articleViewModel.AuthorsIds;
        }

        public ArticleViewModel(IGrouping<string, ArticleResponseModel> articleResponseModels)
        {
            Id = articleResponseModels.Key;
            Name = articleResponseModels.First().ArticleName;
            Date = articleResponseModels.First().Date;
            JournalsIds = new List<string>();
            JournalsNames = new List<string>();
            AuthorsIds = new List<string>();
            AuthorsNames = new List<string>();
            foreach (var item in articleResponseModels)
            {
                if (JournalsIds.Contains(item.JournalId) || item.JournalId == null)
                {
                    continue;
                }
                JournalsIds.Add(item.JournalId);
                JournalsNames.Add(item.JournalName);
            }
            foreach (var item in articleResponseModels)
            {
                if (AuthorsIds.Contains(item.AuthorId) || item.AuthorId == null)
                {
                    continue;
                }
                AuthorsIds.Add(item.AuthorId);
                AuthorsNames.Add(item.AuthorName);
            }
        }
    }
}
