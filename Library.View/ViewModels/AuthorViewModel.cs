﻿using Library.Core.Entities;
using Library.Core.ResponseModels;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace Library.View.ViewModels
{
    public class AuthorViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<string> BooksIds { get; set; }
        public List<string> BooksNames { get; set; }
        public List<string> ArticlesIds { get; set; }
        public List<string> ArticlesNames { get; set; }

        public AuthorViewModel(Author author)
        {
            Id = author.Id;
            Name = author.Name;
        }

        public AuthorViewModel(JObject author)
        {
            bool valueExists = true;
            valueExists = author.TryGetValue("Id", out JToken value);
            Id = author.TryGetValue("Id", out value) ? author["Id"].Value<string>() : "";
            Name = author.TryGetValue("Name", out value) ? author["Name"].Value<string>() : "";
            bool someValue = author.TryGetValue("BooksIds", out value);
            JArray booksIds = author.TryGetValue("BooksIds", out value) ? author["BooksIds"].Value<JArray>() : null;
            BooksIds = booksIds?.ToObject<List<string>>();
            JArray articlesIds = author.TryGetValue("ArticlesIds", out value) ? author["ArticlesIds"].Value<JArray>() : null;
            ArticlesIds = articlesIds?.ToObject<List<string>>();
        }

        public AuthorViewModel(AuthorViewModel authorViewModel)
        {
            Id = authorViewModel.Id;
            Name = authorViewModel.Name;
            BooksIds = authorViewModel.BooksIds;
            ArticlesIds = authorViewModel.ArticlesIds;
        }

        public AuthorViewModel(IGrouping<string, AuthorResponseModel> authorResponseModel)
        {
            Id = authorResponseModel.Key;
            Name = authorResponseModel.First().AuthorName;
            BooksIds = new List<string>();
            BooksNames = new List<string>();
            ArticlesIds = new List<string>();
            ArticlesNames = new List<string>();
            foreach (var item in authorResponseModel)
            {
                if (BooksIds.Contains(item.BookId) || item.BookId == null)
                {
                    continue;
                }
                BooksIds.Add(item.BookId);
                BooksNames.Add(item.BookName);
            }
            foreach (var item in authorResponseModel)
            {
                if (ArticlesIds.Contains(item.ArticleId) || item.ArticleId == null)
                {
                    continue;
                }
                ArticlesIds.Add(item.ArticleId);
                ArticlesNames.Add(item.ArticleName);
            }
        }
    }
}
