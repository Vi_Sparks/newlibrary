﻿using System;
using System.Collections.Generic;
using Library.Core.Entities;
using Newtonsoft.Json.Linq;
using System.Linq;
using Library.Core.ResponseModels;

namespace Library.View.ViewModels
{
    public class JournalViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int CountOfPages { get; set; }
        public float Price { get; set; }
        public DateTime Date { get; set; }
        public string Content { get; set; }
        public List<string> ArticlesIds { get; set; }
        public List<string> ArticlesNames { get; set; }

        public JournalViewModel(Journal journal)
        {
            Id = journal.Id;
            Name = journal.Name;
            Price = journal.Price;
            Date = journal.Date;
        }

        public JournalViewModel(IGrouping<string, JournalResponseModel> journalResponseModels)
        {
            Id = journalResponseModels.Key;
            Name = journalResponseModels.First().JournalName;
            CountOfPages = journalResponseModels.First().CountOfPages;
            Price = journalResponseModels.First().Price;
            Date = journalResponseModels.First().Date;
            Content = journalResponseModels.First().Content;
            ArticlesIds = new List<string>();
            ArticlesNames = new List<string>();
            foreach (var item in journalResponseModels)
            {
                if (ArticlesIds.Contains(item.ArticleId) || item.ArticleId == null)
                {
                    continue;
                }
                ArticlesIds.Add(item.ArticleId);
                ArticlesNames.Add(item.ArticleName);
            }
        }

        public JournalViewModel(JObject journal)
        {
            bool valueExists = true;
            valueExists = journal.TryGetValue("Id", out JToken value);
            Id = journal.TryGetValue("Id", out value) ? journal["Id"].Value<string>() : "";
            Name = journal.TryGetValue("Name", out value) ? journal["Name"].Value<string>() : "";
            Price = journal.TryGetValue("Price", out value) ? journal["Price"].Value<float>() : 0f;
            Date = journal.TryGetValue("Date", out value) ? journal["Date"].Value<DateTime>() : DateTime.Now;
            JArray articlesIds = journal.TryGetValue("ArticlesIds", out value) ? journal["ArticlesIds"].Value<JArray>() : null;
            ArticlesIds = articlesIds?.ToObject<List<string>>();
        }
    }
}
