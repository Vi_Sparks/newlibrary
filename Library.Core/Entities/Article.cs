﻿using System;
namespace Library.Core.Entities
{
    public class Article : BaseEntity
    {
        public DateTime Date { get; set; }
        public string Content { get; set; }
    }
}
