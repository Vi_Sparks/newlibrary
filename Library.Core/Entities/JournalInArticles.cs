﻿using System.ComponentModel.DataAnnotations.Schema;
namespace Library.Core.Entities
{
    public class JournalInArticles : BaseEntity
    {
        public string JournalId { get; set; }
        [ForeignKey("JournalId")]
        public virtual Journal Journal { get; set; }
        public string ArticleId { get; set; }
        [ForeignKey("ArticleId")]
        public virtual Article Article { get; set; }
    }
}
