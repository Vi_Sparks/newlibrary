﻿using System;
namespace Library.Core.Entities
{
    public class Journal : BaseEntity
    {
        public int CountOfPages { get; set; }
        public float Price { get; set; }
        public DateTime Date { get; set; }
        public string Content { get; set; }
    }
}
