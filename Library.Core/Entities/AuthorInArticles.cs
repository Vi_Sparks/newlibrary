﻿using System.ComponentModel.DataAnnotations.Schema;
namespace Library.Core.Entities
{
    public class AuthorInArticles : BaseEntity
    {
        public string AuthorId { get; set; }
        [ForeignKey("AuthorId")]
        public virtual Author Author { get; set; }
        public string ArticleId { get; set; }
        [ForeignKey("ArticleId")]
        public virtual Article Article { get; set; }
    }
}
