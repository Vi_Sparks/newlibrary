﻿namespace Library.Core.Entities
{
    public class Book : BaseEntity
    {
        public int CountOfPages { get; set; }
        public int Year { get; set; }
        public float Price { get; set; }
        public string Genre { get; set; }
        public string Content { get; set; }
    }
}
