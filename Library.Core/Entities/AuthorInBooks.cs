﻿using System.ComponentModel.DataAnnotations.Schema;
namespace Library.Core.Entities
{
    public class AuthorInBooks : BaseEntity
    {
        public string AuthorId { get; set; }
        [ForeignKey("AuthorId")]
        public virtual Author Author { get; set; }
        public string BookId { get; set; }
        [ForeignKey("BookId")]
        public virtual Book Book { get; set; }
    }
}
