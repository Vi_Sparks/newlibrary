﻿namespace Library.Core.ResponseModels
{
    public class AuthorResponseModel
    {
        public string BookId { get; set; }
        public string BookName { get; set; }
        public string AuthorId { get; set; }
        public string AuthorName { get; set; }
        public string ArticleId { get; set; }
        public string ArticleName { get; set; }
    }
}
