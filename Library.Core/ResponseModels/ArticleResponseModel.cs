﻿using System;

namespace Library.Core.ResponseModels
{
    public class ArticleResponseModel
    {
        public string ArticleId { get; set; }
        public string ArticleName { get; set; }
        public DateTime Date { get; set; }
        public string JournalId { get; set; }
        public string JournalName { get; set; }
        public string AuthorId { get; set; }
        public string AuthorName { get; set; }
    }
}
