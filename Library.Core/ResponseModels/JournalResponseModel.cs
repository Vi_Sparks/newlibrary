﻿using System;

namespace Library.Core.ResponseModels
{
    public class JournalResponseModel
    {
        public string ArticleId { get; set; }
        public string ArticleName { get; set; }
        public string JournalId { get; set; }
        public string JournalName { get; set; }
        public int CountOfPages { get; set; }
        public float Price { get; set; }
        public DateTime Date { get; set; }
        public string Content { get; set; }
    }
}
