﻿namespace Library.Core.ResponseModels
{
    public class BookResponseModel
    {
        public string BookId { get; set; }
        public string BookName { get; set; }
        public string AuthorId { get; set; }
        public string AuthorName { get; set; }
        public int CountOfPages { get; set; }
        public int Year { get; set; }
        public float Price { get; set; }
        public string Genre { get; set; }
        public string Content { get; set; }
    }
}
