﻿using System.Data.Entity;
using Library.Core.Entities;
namespace Library.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Journal> Journals { get; set; }
        public DbSet<AuthorInBooks> AuthorInBooks { get; set; }
        public DbSet<AuthorInArticles> AuthorInArticles { get; set; }
        public DbSet<JournalInArticles> JournalInArticles { get; set; }
        public DataContext() : base("LibraryDatabase")
        {
        }
        public DataContext(string connectionString) : base(connectionString)
        {
        }
    }
}
