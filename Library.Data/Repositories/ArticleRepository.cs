﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using Library.Core.Entities;
using Library.Core.ResponseModels;
namespace Library.Data.Repositories
{
    public class ArticleRepository
    {
        protected readonly IDbConnection _connection;

        public ArticleRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<string> SelectArticlesNamesByJournalId(string id)
        {
            string query = @"SELECT Articles.Name 
                             FROM Articles
                             INNER JOIN AuthorInArticles ON AuthorInArticles.ArticleId = Articles.Id
                             WHERE Articles.Id = @id";
            IEnumerable<string> result = _connection.Query<string>(query, new { id = id });
            return result;
        }

        public IEnumerable<string> SelectArticlesIdsByJournalId(string id)
        {
            string query = @"SELECT Articles.Id
                             FROM Articles
                             INNER JOIN JournalInArticles ON JournalInArticles.ArticleId = Articles.Id
                             WHERE JournalInArticles.JournalId = @id";
            IEnumerable <string> result = _connection.Query<string>(query, new { id = id });
            return result;
        }

        public IEnumerable<string> SelectArticlesNamesByAuthorId(string id)
        {
            string query = @"SELECT Articles.Name
                             FROM AuthorInArticles
                             INNER JOIN Articles ON AuthorInArticles.ArticleId = Articles.Id
                             WHERE AuthorInArticles.AuthorId = @id";
            IEnumerable<string> result = _connection.Query<string>(query, new { id = id });
            return result;
        }

        public IEnumerable<string> SelectArticlesIdsByAuthorId(string id)
        {
            string query = @"SELECT Articles.Id 
                             FROM AuthorInArticles
                             INNER JOIN Articles ON AuthorInArticles.ArticleId = Articles.Id
                             WHERE AuthorInArticles.AuthorId = @id";
            IEnumerable <string> result =_connection.Query<string>(query, new { id = id });
            return result;
        }

        public IEnumerable<Article> SelectAll()
        {
            string query = @"SELECT Id, Name, CreationDate, Date, Content
                             FROM Articles";
            IEnumerable<Article> result = _connection.Query<Article>(query);
            return result;
        }

        public IEnumerable<ArticleResponseModel> SelectArticlesWithJournals()
        {
            string query = @"SELECT Articles.Id AS ArticleId, Articles.Name AS ArticleName, Articles.Date,
                                    Journals.Id AS JournalId, Journals.Name AS JournalName,
                                    Authors.Id AS AuthorId, Authors.Name AS AuthorName
                             FROM Articles
                             FULL OUTER JOIN JournalInArticles ON JournalInArticles.ArticleId = Articles.Id
                             LEFT JOIN Journals ON JournalInArticles.JournalId = Journals.Id
                             FULL OUTER JOIN AuthorInArticles ON AuthorInArticles.ArticleId = Articles.Id
                             LEFT JOIN Authors ON AuthorInArticles.AuthorId = Authors.Id;";
            IEnumerable<ArticleResponseModel> result = _connection.Query<ArticleResponseModel>(query);
            return result;
        }

        public bool Insert(Article entity)
        {
            string query = @"INSERT INTO Articles(Id, Name, CreationDate, Date, Content)
                             VALUES(@Id, @Name, @CreationDate, @Date, @Content)";
            int affectedRowsCount = _connection.Execute(query, entity);
            return affectedRowsCount > 0;
        }

        public bool Update(Article entity)
        {
            string query = @"UPDATE Articles 
                             SET Name = @Name,Date = @Date
                             WHERE Id = @Id";
            int affectedRowsCount = _connection.Execute(query, entity);
            return affectedRowsCount > 0;
        }

        public bool Delete(string id)
        {
            string query = @"DELETE 
                             FROM Articles 
                             WHERE Id = @id";
            int affectedRows = _connection.Execute(query, new { id = id });
            return affectedRows > 0;
        }

        public Article Select(string id)
        {
            string query = @"SELECT Id, Name, CreationDate, Date, Content
                             FROM Articles 
                             WHERE Id = @id";
            Article result = _connection.QueryFirst<Article>(query, new { id = id });
            return result;
        }
    }
}
