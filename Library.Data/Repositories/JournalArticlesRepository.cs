﻿using Dapper;
using Library.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data;
namespace Library.Data.Repositories
{
    public class JournalInArticlesRepository
    {
        protected readonly IDbConnection _connection;

        public JournalInArticlesRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<JournalInArticles> SelectJournalWithArticlesRelations(string journalId)
        {
            string query = @"SELECT Id, Name, CreationDate, JournalId, ArticleId 
                             FROM JournalInArticles
                             WHERE JournalId = @journalId";
            IEnumerable <JournalInArticles> result =_connection.Query<JournalInArticles>(query, new { journalId = journalId });
            return result;
        }

        public IEnumerable<JournalInArticles> SelectArticleWithJournalsRelations(string articleid)
        {
            string query = @"SELECT Id, Name, CreationDate, ArticleId, JournalId
                             FROM JournalInArticles
                             WHERE ArticleId = @articleid";
            IEnumerable <JournalInArticles> result = _connection.Query<JournalInArticles>(query, new { articleid = articleid });
            return result;
        }

        public IEnumerable<JournalInArticles> SelectAll()
        {
            string query = @"SELECT Id, Name, CreationDate, JournalId, ArticleId
                             FROM JournalInArticles";
            IEnumerable <JournalInArticles> result = _connection.Query<JournalInArticles>(query);
            return result;
        }

        public bool Insert(JournalInArticles journalArticle)
        {
            string query = @"INSERT INTO JournalInArticles(Id, Name, CreationDate, JournalId, ArticleId)
                             VALUES(@Id, @Name, @CreationDate, @JournalId, @ArticleId)";
            int affectedRowsCount = _connection.Execute(query, journalArticle);
            return affectedRowsCount > 0;
        }

        public bool Insert(List<JournalInArticles> JournalInArticles)
        {
            string query = @"INSERT INTO JournalInArticles(Id, Name, CreationDate, JournalId, ArticleId)
                             VALUES(@Id, @Name, @CreationDate, @JournalId, @ArticleId)";
            int affectedRowsCount = _connection.Execute(query, JournalInArticles);
            return affectedRowsCount > 0;
        }

        public bool Edit(JournalInArticles journalArticle)
        {
            string query = @"UPDATE JournalInArticles 
                             SET Name = @Name, Journal= @Journal, JournalId = @JournalId,
                                 ArticleId = @ArticleId, Article = @Article
                             WHERE Id = @Id";
            int affectedRowsCount = _connection.Execute(query, journalArticle);
            return affectedRowsCount > 0;
        }

        public bool Delete(string journalArticleId)
        {
            string query = @"DELETE
                             FROM JournalInArticles 
                             WHERE Id = @journalArticleId";
            int affectedRows = _connection.Execute(query, new { journalArticleId = journalArticleId });
            return affectedRows > 0;
        }

        public bool DeleteArticleWithJournalsRelations(string articleId)
        {
            string query = @"DELETE
                             FROM JournalInArticles
                             WHERE ArticleId = @articleId";
            var affectedRowsCount = _connection.Execute(query, new { articleId = articleId });
            return affectedRowsCount > 0;
        }

        public bool DeleteJournalWithArticlesRelations(string journalId)
        {
            string query = @"DELETE
                             FROM JournalInArticles
                             WHERE JournalId = @journalId";
            var affectedRowsCount = _connection.Execute(query, new { journalId = journalId });
            return affectedRowsCount > 0;
        }

        public bool DeleteJournalWithArticlesRelations(string journalId, List<string> articlesIds)
        {
            string query = @"DELETE 
                             FROM JournalInArticles
                             WHERE JournalId = @journalId AND ArticleId IN @articlesIds";
            int affectedRowsCount = _connection.Execute(query, new { journalId = journalId, articlesIds = articlesIds });
            return affectedRowsCount > 0;
        }

        public bool DeleteArticleWithJournalsRelations(string articleId, List<string> journalsIds)
        {
            string query = @"DELETE 
                             FROM JournalInArticles 
                             WHERE ArticleId = @articleId AND JournalId IN @journalsIds";
            int affectedRowsCount = _connection.Execute(query, new { articleId = articleId, journalsIds = journalsIds });
            return affectedRowsCount > 0;
        }

        public JournalInArticles Select(string journalArticleId)
        {
            string query = @"SELECT Id, Name, CreationDate, JournalId, ArticleId
                             FROM JournalInArticles 
                             WHERE Id = @journalArticleId";
            JournalInArticles result = _connection.QueryFirst<JournalInArticles>(query, new { journalArticleId = journalArticleId });
            return result;
        }
    }
}
