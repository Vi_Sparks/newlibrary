﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Library.Core.Entities;
using Library.Core.ResponseModels;
namespace Library.Data.Repositories
{
    public class JournalRepository
    {
        IDbConnection _connection;

        public JournalRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<string> SelectJournalsNamesByArticleId(string id)
        {
            string query = @"SELECT Journals.Name
                             FROM Journals
                             INNER JOIN JournalInArticles ON JournalInArticles.JournalId = Journals.Id
                             WHERE JournalInArticles.ArticleId = @id";
            IEnumerable <string> result = _connection.Query<string>(query, new { id = id });
            return result;
        }

        public IEnumerable<string> SelectJournalsIdsByArticleId(string id)
        {
            string query = @"SELECT Journals.Id
                             FROM Journals
                             INNER JOIN JournalInArticles ON JournalInArticles.JournalId = Journals.Id
                             WHERE JournalInArticles.ArticleId = @id";
            IEnumerable <string> result = _connection.Query<string>(query, new { id = id });
            return result;
        }

        public IEnumerable<Journal> SelectAll()
        {
            string query = @"SELECT Id, Name, CreationDate
                             FROM Journals";
            IEnumerable <Journal> result = _connection.Query<Journal>(query);
            return result;
        }

        public IEnumerable<JournalResponseModel> SelectJournalsWithArticles()
        {
            string query = @"SELECT Journals.Id AS JournalId, Journals.Name AS JournalName,
                                    Journals.CountOfPages, Journals.Price, Journals.Date, 
                                    Journals.Content,
                                    Articles.Id AS ArticleId, Articles.Name AS ArticleName
                             FROM JournalInArticles
                             RIGHT OUTER JOIN Journals ON JournalInArticles.JournalId = Journals.Id
                             LEFT OUTER JOIN Articles ON JournalInArticles.ArticleId = Articles.Id";
            var result = _connection.Query<JournalResponseModel>(query);
            return result;
        }

        public bool Insert(Journal journal)
        {
            string query = @"INSERT INTO Journals(Id, Name, CreationDate, CountOfPages, Price, Date, Content)
                             VALUES(@Id, @Name, @CreationDate, @CountOfPages, @Price, @Date, @Content)";
            int affectedRowsCount = _connection.Execute(query, journal);
            return affectedRowsCount > 0;
        }

        public bool Update(Journal journal)
        {
            string query = @"UPDATE Journals 
                            SET Name = @Name, CountOfPages = @CountOfPages,Price = @Price,
                                Date = @Date, Content = @Content
                            WHERE Id = @Id";
            int affectedRowsCount = _connection.Execute(query, journal);
            return affectedRowsCount > 0;
        }

        public bool Delete(string id)
        {
            string query = @"DELETE
                             FROM Journals
                             WHERE Id = @Id";
            int affectedRowsCount = _connection.Execute(query, new { Id = id });
            return affectedRowsCount > 0;
        }

        public Journal Select(string id)
        {
            string query = @"SELECT Id, Name, CreationDate
                             FROM Journals
                             WHERE Id = @id";
            var result = _connection.QueryFirst<Journal>(query, new { id = id });
            return result;
        }
    }
}
