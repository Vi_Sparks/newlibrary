﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using Library.Core.Entities;
using Library.Core.ResponseModels;
using static Dapper.SqlMapper;

namespace Library.Data.Repositories
{
    public class AuthorRepository
    {
        private IDbConnection _connection;

        public AuthorRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<string> SelectAuthorsNamesByBookId(string id)
        {
            string query = @"SELECT Authors.Name
                             FROM Authors
                             INNER JOIN AuthorInBooks ON AuthorInBooks.AuthorId = Authors.Id
                             WHERE AuthorInBooks.BookId = @id";
            IEnumerable <string> result = _connection.Query<string>(query, new { id = id });
            return result;
        }

        public IEnumerable<string> SelectAuthorsIdsByBookId(string id)
        {
            string query = @"SELECT Authors.Id
                             FROM Authors
                             INNER JOIN AuthorInBooks ON AuthorInBooks.AuthorId = Authors.Id
                             WHERE AuthorInBooks.BookId = @id";
            IEnumerable<string> result = _connection.Query<string>(query, new { id = id });
            return result;
        }

        public IEnumerable<string> SelectAuthorsNamesByArticleId(string id)
        {
            string query = @"SELECT Authors.Name
                             FROM Authors
                             INNER JOIN AuthorInArticles ON AuthorInArticles.AuthorId = Authors.Id
                             WHERE AuthorInArticles.ArticleId = @id";
            IEnumerable<string> result = _connection.Query<string>(query, new { id = id });
            return result;
        }

        public IEnumerable<string> SelectAuthorsIdsByArticleId(string id)
        {
            string query = @"SELECT Authors.Id
                             FROM Authors
                             INNER JOIN AuthorInArticles ON AuthorInArticles.AuthorId = Authors.Id
                             WHERE AuthorInArticles.ArticleId = @id";
            IEnumerable<string> result = _connection.Query<string>(query, new { id = id });
            return result;
        }

        public IEnumerable<Author> SelectAll()
        {
            string query = @"SELECT Id, Name, CreationDate
                             FROM Authors";
            IEnumerable<Author> result = _connection.Query<Author>(query);
            return result;
        }

        public IEnumerable<AuthorResponseModel> SelectAuthorsWithBooksAndArticles()
        {
            string query = @"SELECT Authors.Id AS AuthorId, Authors.Name AS AuthorName,
                                    Books.Id AS BookId, Books.Name AS BookName,
                                    Articles.Id AS ArticleId, Articles.Name AS ArticleName
                             FROM Authors
                             FULL OUTER JOIN AuthorInBooks ON AuthorInBooks.AuthorId = Authors.Id
                             LEFT JOIN Books ON AuthorInBooks.BookId = Books.Id
                             FULL OUTER JOIN AuthorInArticles ON AuthorInArticles.AuthorId = Authors.Id
                             LEFT JOIN Articles ON AuthorInArticles.ArticleId = Articles.Id;";
            IEnumerable<AuthorResponseModel> result = _connection.Query<AuthorResponseModel>(query);
            return result;
        }

        public bool Insert(Author author)
        {
            string query = @"INSERT INTO Authors(Id, Name, CreationDate) 
                             VALUES(@Id, @Name, @CreationDate)";
            var affectedRowscount = _connection.Execute(query, author);
            return affectedRowscount > 0;
        }

        public bool Update(Author author)
        {
            string query = @"UPDATE Authors
                             SET Name = @Name
                             WHERE Id = @Id";
            var affectedRowscount = _connection.Execute(query, author);
            return affectedRowscount > 0;
        }

        public bool Delete(string authorId)
        {
            string query = @"DELETE
                             FROM Authors 
                             WHERE Id = @authorId";
            var affectedRowsCount = _connection.Execute(query, new { authorId = authorId });
            return affectedRowsCount > 0;
        }

        public Author SelectById(string id)
        {
            string query = @"SELECT Id, Name, CreationDate
                             FROM Authors
                             WHERE Id = @id";
            Author result = _connection.QueryFirst<Author>(query, new { id = id });
            return result;
        }

        public Author SelectByName(string name)
        {
            string query = @"SELECT Id, Name, CreationDate
                             FROM Authors
                             WHERE Name = @Name";
            Author result = _connection.QueryFirst<Author>(query, new { Name = name });
            return result;
        }
    }
}
