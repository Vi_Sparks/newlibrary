﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using Library.Core.Entities;
namespace Library.Data.Repositories
{
    public class AuthorInArticlesRepository
    {
        protected readonly IDbConnection _connection;

        public AuthorInArticlesRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<AuthorInArticles> SelectArticleWithAuthorsRelations(string id)
        {
            string query = @"SELECT Id, Name, AuthorId, ArticleId
                             FROM AuthorInArticles
                             WHERE ArticleId=@id";
            IEnumerable<AuthorInArticles> result = _connection.Query<AuthorInArticles>(query, new { id = id });
            return result;
        }

        public IEnumerable<AuthorInArticles> SelectAuthorWithArticlesRelations(string id)
        {
            string query = @"SELECT Id, Name, AuthorId, ArticleId
                             FROM AuthorInArticles
                             WHERE AuthorId=@id";
            IEnumerable<AuthorInArticles> result = _connection.Query<AuthorInArticles>(query, new { id = id });
            return result;
        }

        public IEnumerable<AuthorInArticles> SelectAll()
        {
            string query = @"SELECT Id, Name, CreationDate, AuthorId, ArticleId
                             FROM AuthorInArticles";
            IEnumerable<AuthorInArticles> result = _connection.Query<AuthorInArticles>(query);
            return result;
        }

        public bool Insert(AuthorInArticles authorArticle)
        {
            string query = @"INSERT INTO AuthorInArticles(Id, Name, CreationDate, AuthorId, ArticleId) 
                             VALUES(@Id, @Name, @CreationDate, @AuthorId, @ArticleId)";
            int affectedRowsCount = _connection.Execute(query, authorArticle);
            return affectedRowsCount > 0;
        }

        public bool Insert(List<AuthorInArticles> AuthorInArticles)
        {
            string query = @"INSERT INTO AuthorInArticles(Id, Name, CreationDate, AuthorId, ArticleId) 
                             VALUES(@Id, @Name, @CreationDate, @AuthorId, @ArticleId)";
            int affectedRowsCount = _connection.Execute(query, AuthorInArticles);
            return affectedRowsCount > 0;
        }

        public bool Update(AuthorInArticles authorArticle)
        {
            string query = @"UPDATE JournalInArticles 
                             SET Name = @Name, AuthorId = @AuthorId, Author = @Author,
                                 ArticleId = @ArticleId, Article = @Article,
                             WHERE Id = @Id";
            int affectedRowsCount = _connection.Execute(query, authorArticle);
            return affectedRowsCount > 0;
        }

        public bool Delete(string authorArticleId)
        {
            string query = @"DELETE 
                             FROM AuthorInArticles 
                             WHERE Id=@authorArticleId";
            int affectedRowsCount = _connection.Execute(query, new { authorArticleId = authorArticleId });
            return affectedRowsCount > 0;
        }

        public bool DeleteAuthorWithArticlesRelations(string authorId)
        {
            string query = @"DELETE 
                             FROM AuthorInArticles 
                             WHERE AuthorId = @authorId";
            int affectedRowsCount = _connection.Execute(query, new { authorId = authorId});
            return affectedRowsCount > 0;
        }

        public bool DeleteArticleWithAuthorsRelations(string articleId)
        {
            string query = @"DELETE 
                             FROM AuthorInArticles 
                             WHERE ArticleId = @articleId";
            int affectedRowsCount = _connection.Execute(query, new { articleId = articleId });
            return affectedRowsCount > 0;
        }

        public bool DeleteArticleWithAuthorsRelations(string articleId, List<string> authorsIds)
        {
            string query = @"DELETE 
                             FROM AuthorInArticles 
                             WHERE ArticleId = @articleId AND AuthorId IN @authorsIds";
            int affectedRowsCount = _connection.Execute(query, new
            {
                articleId = articleId,
                authorsIds = authorsIds
            });
            return affectedRowsCount > 0;
        }

        public bool DeleteAuthorWithArticlesRelations(string authorId, List<string> articlesIds)
        {
            string query = @"DELETE 
                             FROM AuthorInArticles 
                             WHERE AuthorId = @authorId AND ArticleId IN @articlesIds";
            int affectedRowsCount = _connection.Execute(query, new
            {
                authorId = authorId,
                articlesIds = articlesIds
            });
            return affectedRowsCount > 0;
        }

        public bool Delete(List<AuthorInArticles> AuthorInArticles)
        {
            string query = @"DELETE
                             FROM AuthorInArticles 
                             WHERE Id = @Id";
            int affectedRowsCount = _connection.Execute(query, AuthorInArticles);
            return affectedRowsCount > 0;
        }

        public AuthorInArticles Select(string authorArticleId)
        {
            string query = @"SELECT Id, Name, CreationDate, AuthorId, ArticleId
                             FROM AuthorInArticles 
                             WHERE Id = @authorArticleId";
            AuthorInArticles result = _connection.QueryFirst<AuthorInArticles>(query, new { authorArticleId = authorArticleId });
            return result;
        }
    }
}
