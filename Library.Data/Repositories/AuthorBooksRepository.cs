﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using Library.Core.Entities;
namespace Library.Data.Repositories
{
    public class AuthorInBooksRepository
    {
        IDbConnection _connection;

        public AuthorInBooksRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<AuthorInBooks> SelectAuthorWithBooksRelations(string id)
        {
            string query = @"SELECT Id, Name, CreationDate, AuthorId, BookId
                             FROM AuthorInBooks
                             WHERE AuthorId=@id";
            IEnumerable<AuthorInBooks> result = _connection.Query<AuthorInBooks>(query, new { id = id });
            return result;
        }

        public IEnumerable<AuthorInBooks> SelectAll()
        {
            string query = @"SELECT Id, Name, CreationDate, AuthorId, BookId
                             FROM AuthorInBooks";
            IEnumerable<AuthorInBooks> result = _connection.Query<AuthorInBooks>(query);
            return result;
        }

        public bool Insert(AuthorInBooks entity)
        {
            string query = @"INSERT INTO AuthorInBooks(Id, Name, CreationDate, AuthorId, BookId) 
                             VALUES(@Id, @Name, @CreationDate, @AuthorId, @BookId)";
            int affectedRowsCount = _connection.Execute(query, entity);
            return affectedRowsCount > 0;
        }

        public bool Insert(List<AuthorInBooks> AuthorInBooks)
        {
            string query = @"INSERT INTO AuthorInBooks(Id, Name, CreationDate, AuthorId, BookId)
                             VALUES(@Id, @Name, @CreationDate, @AuthorId, @BookId)";
            int affectedRowsCount = _connection.Execute(query, AuthorInBooks);
            return affectedRowsCount > 0;
        }

        public bool Update(AuthorInBooks entity)
        {
            string query = @"UPDATE AuthorInBooks 
                             SET Name = @Name, AuthorId = @AuthorId, Author = @Author,
                                 BookId = @BookId, Book = @Book
                             WHERE Id = @Id";
            int affectedRowsCount = _connection.Execute(query, entity);
            return affectedRowsCount > 0;
        }

        public bool Delete(string id)
        {
            string query = @"DELETE 
                             FROM AuthorInBooks
                             WHERE Id = @Id";
            int affectedRowsCount = _connection.Execute(query, new { Id = id });
            return affectedRowsCount > 0;
        }

        public bool Delete(List<AuthorInBooks> AuthorInBooks)
        {
            string query = @"DELETE
                             FROM AuthorInBooks
                             WHERE Id = @Id";
            int affectedRowsCount = _connection.Execute(query, AuthorInBooks);
            return affectedRowsCount > 0;
        }

        public bool DeleteBookWithAuthorsRelations(string bookId)
        {
            string query = @"DELETE
                             FROM AuthorInBooks
                             WHERE BookId = @bookId";
            int affectedRowsCount = _connection.Execute(query, new { bookId = bookId });
            return affectedRowsCount > 0;
        }

        public bool DeleteAuthorWithBooksRelations(string authorId)
        {
            string query = @"DELETE
                             FROM AuthorInBooks
                             WHERE AuthorId = @authorId";
            int affectedRowsCount = _connection.Execute(query, new { authorId = authorId });
            return affectedRowsCount > 0;
        }

        public bool DeleteBookWithAuthorsRelations(string bookId, List<string> authorsIds)
        {
            string query = @"DELETE
                             FROM AuthorInBooks
                             WHERE BookId = @bookId AND AuthorId IN @authorsIds";
            int affectedRowsCount = _connection.Execute(query, new { bookId = bookId, authorsIds = authorsIds });
            return affectedRowsCount > 0;
        }

        public bool DeleteAuthorWithBooksRelations(string authorId, List<string> booksIds)
        {
            string query = @"DELETE 
                             FROM AuthorInBooks 
                             WHERE AuthorId = @authorId AND BookId IN @booksIds";
            int affectedRowsCount = _connection.Execute(query, new { authorId = authorId, booksIds = booksIds });
            return affectedRowsCount > 0;
        }

        public AuthorInBooks Select(string id)
        {
            string query = @"SELECT Id, Name, CreationDate, AuthorId, BookId
                             FROM AuthorInBooks
                             WHERE Id=@Id";
            AuthorInBooks result = _connection.QueryFirst<AuthorInBooks>(query, new { Id = id });
            return result;
        }
    }
}
