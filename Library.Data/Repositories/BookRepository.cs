﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using static Dapper.SqlMapper;
using Library.Core.Entities;
using Library.Core.ResponseModels;

namespace Library.Data.Repositories
{
    public class BookRepository
    {
        private IDbConnection _connection;

        public BookRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<string> SelectBooksIds(string authorId)
        {
            string query = @"SELECT Books.Id
                             FROM AuthorInBooks
                             INNER JOIN Books ON AuthorInBooks.BookId = Books.Id
                             WHERE AuthorInBooks.AuthorId = @authorId";
            IEnumerable<string> result = _connection.Query<string>(query, new { authorId = authorId });
            return result;
        }

        public IEnumerable<string> SelectBooksNames(string authorId)
        {
            string query = @"SELECT Books.Name
                             FROM Books
                             INNER JOIN AuthorInBooks ON AuthorInBooks.BookId = Books.Id
                             WHERE AuthorInBooks.AuthorId = @authorId";
            IEnumerable<string> result = _connection.Query<string>(query, new { authorId = authorId });
            return result;
        }

        public IEnumerable<Book> SelectAll()
        {
            string query = @"SELECT Books.Id , Books.Name, Books.CountOfPages, Books.Year, Books.Price,
                                    Books.Genre, Books.Content
                             FROM Books";
            IEnumerable<Book> result = _connection.Query<Book>(query);
            return result;
        }

        public IEnumerable<BookResponseModel> SelectBooksWithAuthors()
        {
            string query = @"SELECT Books.Id AS BookId, Books.Name AS BookName,
                                    Books.CountOfPages, Books.Year, Books.Price,
                                    Books.Genre, Books.Content,
                                    Authors.Id AS AuthorId, Authors.Name AS AuthorName
                             FROM AuthorInBooks
                             RIGHT OUTER JOIN Books ON AuthorInBooks.BookId = Books.Id
                             LEFT OUTER JOIN Authors ON AuthorInBooks.AuthorId = Authors.Id";
            var result = _connection.Query<BookResponseModel>(query);
            return result;
        }

        public bool Insert(Book entity)
        {
            string query = @"INSERT INTO Books(Id, Name, CreationDate, CountOfPages, Year, Price, Genre, Content)
                             VALUES(@Id, @Name, @CreationDate, @CountOfPages, @Year, @Price, @Genre, @Content)";
            int affectedRowsCount = _connection.Execute(query, entity);
            return affectedRowsCount > 0;
        }

        public bool Update(Book entity)
        {
            string query = @"UPDATE Books
                             SET Name = @Name, CountOfPages = @CountOfPages, Year = @Year,
                                 Price = @Price, Genre = @Genre, Content = @Content 
                             WHERE Id = @Id";
            int affectedRowsCount = _connection.Execute(query, entity);
            return affectedRowsCount > 0;
        }

        public bool Delete(string id)
        {
            string query = @"DELETE
                             FROM Books
                             WHERE Id = @id";
            int affectedRowsCount = _connection.Execute(query, new { id = id });
            return affectedRowsCount > 0;
        }

        public Book Select(string id)
        {
            string query = @"SELECT Id, Name, CreationDate
                             FROM Books 
                             WHERE Id = @id";
            return _connection.QuerySingle<Book>(query, new { id = id });
        }
    }
}
