import { Component, EventEmitter, OnInit, Input, ElementRef, ApplicationRef, Inject } from '@angular/core';
import { products } from './products';
import { DataService } from './data.service';
import { Product } from '../app/product.model'

@Component({
  styleUrls: ['./app.component.css'],
  selector: 'app-root',
  template: `
    <kendo-autocomplete [data]="listItems"
      [valueField]="'ProductName'"
      [placeholder]="'Search Product...'" >
    </kendo-autocomplete>`,
  providers: [DataService]
})
export class AppComponent {
  public listItems: Array<Product> = [];
  public gridData: any[] = products;
  public webApiEndPoint: string;

  constructor(private elementRef: ElementRef, @Inject(DataService) private dataService: DataService) {

    let native = this.elementRef.nativeElement;
    this.webApiEndPoint = native.getAttribute("webApiEndPoint");
  }
  ngOnInit() {
    this.dataService.fetchData().subscribe((data) => this.listItems = data)
  }
}
