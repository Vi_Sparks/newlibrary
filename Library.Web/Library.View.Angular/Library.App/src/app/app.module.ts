import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GridModule } from '@progress/kendo-angular-grid';
import { AppComponent } from './app.component';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

@NgModule({
  declarations: [ AppComponent ],
  imports: [BrowserModule, BrowserAnimationsModule, GridModule, HttpClientModule, HttpClientJsonpModule, DropDownsModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
