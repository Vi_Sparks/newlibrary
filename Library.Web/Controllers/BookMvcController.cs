﻿using System.Web.Mvc;
namespace Library.Web.Controllers
{
    public class BookMvcController : Controller
    {
        public ActionResult Books()
        {
            return View("Books");
        }
        public ActionResult Authors()
        {
            return View("Authors");
        }
        public ActionResult Journals()
        {
            return View("Journals");
        }
        public ActionResult Articles()
        {
            return View("Articles");
        }
    }
}