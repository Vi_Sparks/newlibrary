﻿using System;
using System.Configuration;
using System.Web.Http;
using System.Collections.Generic;
using Library.Services;
using Library.View.ViewModels;
using Newtonsoft.Json.Linq;
namespace Library.Web.Controllers
{
    [RoutePrefix("api/books")]
    public class BookApiController : ApiController
    {
        protected readonly BookServices bookServices;
        public BookApiController()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["LibraryDatabase"].ToString();
            bookServices = new BookServices(connectionString);
        }
        [Route("ListOfBooks")]
        [HttpGet]
        public IHttpActionResult ListOfBooks()
        {
            IEnumerable<BookViewModel> booksViewModels = bookServices.GetAll();
            return Ok(booksViewModels);
        }

        [Route("ListOfBooksInAuthors")]
        [HttpGet]
        public IHttpActionResult ListOfArticlesInJournals()
        {
            IEnumerable<BookViewModel> bookViewModels = bookServices.GetBooksInAuthors();
            return Ok(bookViewModels);
        }

        [Route("create")]
        [AcceptVerbs("PUT")]
        [HttpPut]
        public IHttpActionResult Create(JObject book)
        {
            if (book == null)
            {
                return BadRequest();
            }
            try
            {
                BookViewModel bookViewModel = bookServices.Create(new BookViewModel(book));
                return Ok(bookViewModel);
            }
            catch (Exception)
            {
                return BadRequest($"There was an error creating book ");
            }
        }
        [Route("update")]
        [HttpPost]
        public IHttpActionResult Update(JObject book)
        {
            if (book == null)
            {
                return BadRequest();
            }
            try
            {
                BookViewModel bookViewModel = bookServices.Update(new BookViewModel(book));
                return Ok(bookViewModel);
            }
            catch (Exception)
            {
                return BadRequest($"There was an error updating book ");
            }
        }
        [Route("delete/{id}")]
        [HttpPost]
        public IHttpActionResult Delete(string id)
        {
            try
            {
                bookServices.Delete(id);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest($"There was an error deleting book with id {id}");
            }
        }
    }
}
