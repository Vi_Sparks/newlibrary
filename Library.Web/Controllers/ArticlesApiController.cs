﻿using System;
using System.Collections.Generic;
using Library.Services;
using System.Web.Http;
using System.Configuration;
using Library.View.ViewModels;
using Newtonsoft.Json.Linq;
namespace Library.Web.Controllers
{
    [RoutePrefix("api/articles")]
    public class ArticlesApiController : ApiController
    {
        protected readonly ArticleServices articleServices;
        public ArticlesApiController()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["LibraryDatabase"].ToString();
            articleServices = new ArticleServices(connectionString);
        }
        [Route("ListOfArticles")]
        [HttpGet]
        public IHttpActionResult ListOfArticles()
        {
            IEnumerable<ArticleViewModel> articlesViewModels = articleServices.GetAll();
            return Ok(articlesViewModels);
        }

        [Route("ListOfArticlesInJournals")]
        [HttpGet]
        public IHttpActionResult ListOfArticlesInJournals()
        {
            IEnumerable<ArticleViewModel> articlesViewModels = articleServices.GetArticlesInJournals();
            return Ok(articlesViewModels);
        }

        [Route("create")]
        [AcceptVerbs("PUT")]
        [HttpPut]
        public IHttpActionResult Create(JObject article)
        {
            if (article == null)
            {
                return BadRequest();
            }
            try
            {
                ArticleViewModel articleViewModel = articleServices.Create(new ArticleViewModel(article));
                return Ok(articleViewModel); 
            }
            catch (Exception)
            {
                return BadRequest($"There was an error creating article ");
            }
        }
        [Route("update")]
        [HttpPost]
        public IHttpActionResult Update(JObject article)
        {
            if (article == null)
            {
                return BadRequest();
            }
            try
            {
                ArticleViewModel articleViewModel = articleServices.Update(new ArticleViewModel(article));
                return Ok(articleViewModel);
            }
            catch (Exception)
            {
                return BadRequest($"There was an error updating article ");
            }
        }
        [Route("delete/{articleId}")]
        [HttpPost]
        public IHttpActionResult Delete(string articleId)
        {
            try
            {
                articleServices.Delete(articleId);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest($"There was an error deleting article with id {articleId}");
            }
        }
    }
}
