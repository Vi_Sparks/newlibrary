﻿using System;
using System.Web.Http;
using System.Configuration;
using System.Collections.Generic;
using Library.Services;
using Library.View.ViewModels;
using Newtonsoft.Json.Linq;
namespace Library.Web.Controllers
{
    [RoutePrefix("api/journals")]
    public class JournalApiController : ApiController
    {
        protected readonly JournalServices journalServices;
        public JournalApiController()
        {
            string coonectionString = ConfigurationManager.ConnectionStrings["LibraryDatabase"].ToString();
            journalServices = new JournalServices(coonectionString);
        }
        [Route("ListOfJournals")]
        [HttpGet]
        public IHttpActionResult ListOfJournals()
        {
            IEnumerable<JournalViewModel> journalsViewModels = journalServices.GetAllJournals();
            return Ok(journalsViewModels);
        }

        [Route("ListOfJournalsInArticles")]
        [HttpGet]
        public IHttpActionResult ListOfJournalsInArticles()
        {

            IEnumerable<JournalViewModel> journalViewModels = journalServices.GetJournalsInArticles();
            return Ok(journalViewModels);
        }

        [Route("create")]
        [AcceptVerbs("PUT")]
        [HttpPut]
        public IHttpActionResult Create(JObject journal)
        {
            if (journal == null)
            {
                return BadRequest();
            }
            try
            {
                JournalViewModel journalViewModel = journalServices.Create(new JournalViewModel(journal));
                return Ok(journalViewModel);
            }
            catch (Exception)
            {
                return BadRequest($"There was an error creating journal ");
            }
        }
        [Route("update")]
        [HttpPost]
        public IHttpActionResult Update(JObject journal)
        {
            if (journal == null)
            {
                return BadRequest();
            }
            try
            {
                JournalViewModel newJournal = journalServices.Update(new JournalViewModel(journal));
                return Ok(newJournal);
            }
            catch (Exception)
            {
                return BadRequest($"There was an error updating book ");
            }
        }
        [Route("delete/{id}")]
        [HttpPost]
        public IHttpActionResult Delete(string id)
        {
            try
            {
                journalServices.Delete(id);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest($"There was an error deleting journal with id {id}");
            }
        }
    }
}
