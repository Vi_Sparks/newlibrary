﻿using Library.Services;
using Library.View.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;
using Newtonsoft.Json.Linq;
namespace Library.Web.Controllers
{
    [RoutePrefix("api/authors")]
    public class AuthorApiController : ApiController
    {
        protected readonly AuthorServices authorServices;
        public AuthorApiController()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["LibraryDatabase"].ToString();
            authorServices = new AuthorServices(connectionString);
        }
        [Route("ListOfAuthors")]
        [HttpGet]
        public IHttpActionResult ListOfAuthors()
        {
            IEnumerable<AuthorViewModel> authorsViewModels = authorServices.GetAll();
            return Ok(authorsViewModels);
        }
        [Route("ListOfAuthorsInBooks")]
        [HttpGet]
        public IHttpActionResult ListOfAuthorsInBooks()
        {
            IEnumerable<AuthorViewModel> authorsViewModels = authorServices.GetAuthorsInBooks();
            return Ok(authorsViewModels);
        }

        [Route("create")]
        [AcceptVerbs("PUT")]
        [HttpPut]
        public IHttpActionResult Create(JObject author)
        {
            if (author == null)
            {
                return BadRequest();
            }
            try
            {
                AuthorViewModel authorViewModel = authorServices.Create(new AuthorViewModel(author));
                return Ok(authorViewModel);
            }
            catch (Exception)
            {
                return BadRequest($"There was an error creating author ");
            }
        }
        [Route("update")]
        [HttpPost]
        public IHttpActionResult Update(JObject author)
        {
            if (author == null)
            {
                return BadRequest();
            }
            try
            {
                AuthorViewModel authorViewModel = authorServices.Update(new AuthorViewModel(author));
                return Ok(authorViewModel);
            }
            catch (Exception)
            {
                return BadRequest($"There was an error updating author ");
            }
        }
        [Route("delete/{authorId}")]
        [HttpPost]
        public IHttpActionResult Delete(string authorId)
        {
            try
            {
                authorServices.Delete(authorId);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest($"There was an error deleting author with id {authorId}");
            }
        }
    }
}
