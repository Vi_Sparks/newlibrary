﻿using System.Web.Optimization;
namespace BundlesApp
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/Site.css"));
            bundles.Add(new StyleBundle("~/Content/kendo-styles").Include(
                      "~/Content/kendo/2017.3.913/kendo.common.min.css",
                      "~/Content/kendo/2017.3.913/kendo.metro.min.css"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/kendo-scripts").Include(
                        "~/Scripts/kendo/2017.3.913/kendo.all.min.js",
                        "~/Scripts/kendo/2017.3.913/kendo.web.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/kendo-custom-scripts").Include(
                        "~/Scripts/library/kendo-library-variables.js"));
            bundles.Add(new ScriptBundle("~/bundles/library-angular").Include(
                        "~/Library.View.Angular/Library.App/dist/inline.bundle.js",
                        "~/Library.View.Angular/Library.App/dist/main.bundle.js",
                        "~/Library.View.Angular/Library.App/dist/polyfills.bundle.js",
                        "~/Library.View.Angular/Library.App/dist/styles.bundle.js",
                        "~/Library.View.Angular/Library.App/dist/vendor.bundle.js"));
        }
    }
}