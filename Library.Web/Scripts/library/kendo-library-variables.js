﻿$(document).ready(
    function () {
        function multiSelectEditor(container, options) {
            $('<input data-bind="value:' + options.field + '"/>').appendTo(container).kendoMultiSelect({
                dataSource: options.values,
                dataValueField: "Id",
                dataTextField: "Name",
                valuePrimitive: true
            });
        }
        function dateTimeEditor(container, options) {
            $('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
                .appendTo(container)
                .kendoDatePicker({});
        }
        function setColumnHeader(columnName) {
            return function () {
                return columnName;
            };
        }
        var authorModel = new kendo.data.Model.define({
            id: "Id",
            fields: {
                Id: { type: "string", editable: false },
                Name: { type: "string", editable: true, validation: { required: true } },
                BooksIds: { defaultValue: [""] },
                ArticlesIds: { defaultValue: [""] }
            }
        });
        var authorsMainGridSource = new kendo.data.DataSource({
            batch: false,
            transport: {
                create: {
                    url: function () {
                        return "/api/authors/create/";
                    },
                    type: "PUT",
                    dataType: "json",
                    contentType: "application/json"
                },
                read: {
                    url: function () {
                        return "/api/authors/ListOfAuthors/";
                    },
                    type: "GET"
                },
                update: {
                    url: function () {
                        return "/api/authors/update/";
                    },
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json"
                },
                destroy: {
                    url: function (data) {
                        return "/api/authors/delete/" + data.Id;
                    },
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json"
                },
                parameterMap: function (data, operation) {
                    if (operation === "create" || operation === "update") {
                        return JSON.stringify(data);
                    }
                    return data;
                }
            },
            schema: {
                data: function (data) {
                    return data;
                },
                total: function (data) {
                    return Object.keys(data).length;

                },
                errors: "Errors",
                error: function (e) {
                    alert(e.errors);
                },
                model: authorModel
            }
        });
        var authorsInBooksDataSource = new kendo.data.DataSource({
            batch: false,
            transport: {
                read: {
                    url: function () {
                        return "/api/authors/ListOfAuthorsInBooks/";
                    },
                    type: "GET"
                },
                parameterMap: function (data, operation) {
                    if (operation === "create" || operation === "update") {
                        return JSON.stringify(data);
                    }
                    return data;
                }
            },
            schema: {
                data: function (data) {
                    return data;
                },
                total: function (data) {
                    return Object.keys(data).length;

                },
                errors: "Errors",
                error: function (e) {
                    alert(e.errors);
                },
                model: bookModel
            }
        });
        var bookModel = new kendo.data.Model.define({
            id: "Id",
            fields: {
                Id: { type: "string", editable: false, nullable: false },
                Name: { type: "string", editable: true, validation: { required: true } },
                Price: { type: "number", editable: true, validation: { required: true } },
                Year: { type: "number", editable: true, validation: { required: true } },
                Genre: { type: "string", editable: true, validation: { required: true } },
                AuthorsIds: { defaultValue: [""] }

            }
        });
        var booksGridSource = new kendo.data.DataSource({
            batch: false,
            transport: {
                create: {
                    url: function () {
                        return "/api/books/create/";
                    },
                    type: "PUT",
                    dataType: "json",
                    contentType: "application/json"
                },
                read: {
                    url: function () {
                        return "/api/books/ListOfBooks/";
                    },
                    type: "GET"
                },
                update: {
                    url: function () {
                        return "/api/books/update/";
                    },
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json"
                },
                destroy: {
                    url: function (e) {
                        return "/api/books/delete/" + e.Id;
                    },
                    type: "POST"
                },
                parameterMap: function (data, operation) {
                    if (operation === "create" || operation === "update") {
                        return JSON.stringify(data);
                    }
                    return data;
                }
            },
            schema: {
                data: function (data) {
                    return data;
                },
                total: function (data) {
                    return Object.keys(data).length;

                },
                errors: "Errors",
                error: function (e) {
                    alert(e.errors);
                },
                model: bookModel
            }
        });
        var booksInAuthorsDataSource = new kendo.data.DataSource({
            batch: false,
            transport: {
                read: {
                    url: function () {
                        return "/api/books/ListOfBooksInAuthors/";
                    },
                    type: "GET"
                },
                parameterMap: function (data, operation) {
                    if (operation === "create" || operation === "update") {
                        return JSON.stringify(data);
                    }
                    return data;
                }
            },
            schema: {
                data: function (data) {
                    return data;
                },
                total: function (data) {
                    return Object.keys(data).length;

                },
                errors: "Errors",
                error: function (e) {
                    alert(e.errors);
                },
                model: bookModel
            }
        });
        var booksGrid = $("#grid").kendoGrid(
            {
                scrollable: false,
                dataSource: booksGridSource,
                toolbar: ["create"],
                schema: bookModel,
                columns:
                    [
                        { field: "Id", type: "string", hidden: true },
                        { field: "Name", type: "string" },
                        { field: "Price", type: "number", format: "{0:c}" },
                        { field: "Year", type: "number", format: "{0: yyyy}" },
                        { field: "Genre", type: "string" },
                        {
                            field: "AuthorsIds", editor: multiSelectEditor, values: authorsInBooksDataSource,
                            headerTemplate: setColumnHeader("Authors"), template: function (item) {
                                if (item !== null && item.Id !== "" && item.AuthorsNames !== null)
                                    return item.AuthorsNames.join(', ');
                                else return "";
                            }
                        },
                        { command: ["edit", "destroy"], title: "&nbsp;" }
                    ],
                editable: "inline"
            });
        var articlesModel = new kendo.data.Model.define({
            id: "Id",
            fields: {
                Id: { type: "string", editable: false, nullable: false },
                Name: { type: "string", editable: true, validation: { required: true } },
                Date: { type: "Date", editable: true },
                JournalsIds: { defaultValue: [""] },
                AuthorsIds: { defaultValue: [""] }
            }
        });
        var articlesGridSource = new kendo.data.DataSource({
            batch: false,
            transport: {
                create: {
                    url: function (e) {
                        e.Id = "";
                        return "/api/articles/create/";
                    },
                    type: "PUT",
                    dataType: "json",
                    contentType: "application/json"
                },
                read: {
                    url: function () {
                        return "/api/articles/ListOfArticles/";
                    },
                    type: "GET"
                },
                update: {
                    url: function () {
                        return "/api/articles/update/";
                    },
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json"
                },
                destroy: {
                    url: function (e) {
                        return "/api/articles/delete/" + e.Id;
                    },
                    type: "POST"
                },
                parameterMap: function (data, operation) {
                    if (operation === "update" || operation === "create") {
                        return JSON.stringify(data);
                    }
                    return data;
                }
            },
            schema: {
                data: function (data) {
                    return data;
                },
                total: function (data) {
                    return Object.keys(data).length;

                },
                errors: "Errors",
                error: function (e) {
                    alert(e.errors);
                },
                model: articlesModel
            }
        });
        var authorsMainGrid = $("#authorsMainGrid").kendoGrid({
            scrollable: false,
            dataSource: authorsMainGridSource,
            toolbar: [{ name: "create", text: "Add new record " }],
            schema: authorModel,
            columns:
                [
                    { field: "Id", type: "string", hidden: true },
                    { field: "Name", type: "string" },
                    {
                        field: "BooksIds", editor: multiSelectEditor, values: booksInAuthorsDataSource,
                        headerTemplate: setColumnHeader("Books"),
                        template: function (item) {
                            if (item !== null && item.Id !== "" && item.BooksNames !== null)
                                return item.BooksNames.join(', ');
                            else return "";
                        }
                    },
                    {
                        field: "ArticlesIds", editor: multiSelectEditor, values: articlesGridSource,
                        headerTemplate: setColumnHeader("Articles"),
                        template: function (item) {
                            if (item !== null && item.Id !== "" && item.ArticlesNames !== null)
                                return item.ArticlesNames.join(', ');
                            else return "";
                        }
                    },
                    {
                        command: [
                            { name: "edit", text: { edit: " ", update: " ", cancel: " " } },
                            { name: "destroy", text: " " }
                        ], title: "&nbsp;"
                    }
                ],
            editable: "inline"
        });
        var journalModel = new kendo.data.Model.define({
            id: "Id",
            fields: {
                Id: { type: "string", editable: false, nullable: false },
                Name: { type: "string", editable: true, validation: { required: true } },
                CountOfPages: { type: "number", editable: true },
                Price: { type: "number", editable: true, validation: { required: true } },
                Date: { type: "Date", editable: true },
                ArticlesIds: { defaultValue: [""] }
            }
        });
        var journalsInArticlesSource = new kendo.data.DataSource({
            batch: false,
            transport: {
                read: {
                    url: function () {
                        return "/api/journals/ListOfJournalsInArticles/";
                    },
                    type: "GET"
                }
            },
            schema: {
                data: function (data) {
                    return data;
                },
                total: function (data) {
                    return Object.keys(data).length;

                },
                errors: "Errors",
                error: function (e) {
                    alert(e.errors);
                },
                model: journalModel
            }
        });
        var articlesInJournalsSource = new kendo.data.DataSource({
            batch: false,
            transport: {
                read: {
                    url: function () {
                        return "/api/articles/ListOfArticlesInJournals/";
                    },
                    type: "GET"
                }
            },
            schema: {
                data: function (data) {
                    return data;
                },
                total: function (data) {
                    return Object.keys(data).length;

                },
                errors: "Errors",
                error: function (e) {
                    alert(e.errors);
                },
                model: articlesModel
            }
        });
        var journalsGridSource = new kendo.data.DataSource({
            batch: false,
            transport: {
                create: {
                    url: function (e) {
                        e.Id = "";
                        return "/api/journals/create/";
                    },
                    type: "PUT",
                    dataType: "json",
                    contentType: "application/json"
                },
                read: {
                    url: function () {
                        return "/api/journals/ListOfJournals/";
                    },
                    type: "GET"
                },
                update: {
                    url: function () {
                        return "/api/journals/update/";
                    },
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json"
                },
                destroy: {
                    url: function (e) {
                        return "/api/journals/delete/" + e.Id;
                    },
                    type: "POST"
                },
                parameterMap: function (data, operation) {
                    if (operation === "update" || operation === "create") {
                        return JSON.stringify(data);
                    }
                    return data;
                }
            },
            schema: {
                data: function (data) {
                    return data;
                },
                total: function (data) {
                    return Object.keys(data).length;

                },
                errors: "Errors",
                error: function (e) {
                    alert(e.errors);
                },
                model: journalModel
            }

        });
        var journalsGrid = $("#journalsGrid").kendoGrid(
            {
                scrollable: false,
                dataSource: journalsGridSource,
                toolbar: ["create"],
                schema: journalModel,
                columns:
                    [
                        { field: "Id", type: "string", hidden: true },
                        { field: "Name", type: "string" },
                        { field: "Price", type: "number", format: "{0:c}" },
                        { field: "Date", type: "Date", format: "{0:MM/dd/yyyy}", editor: dateTimeEditor },
                        {
                            field: "ArticlesIds", editor: multiSelectEditor, values: articlesInJournalsSource,
                            headerTemplate: setColumnHeader("Articles"),
                            template: function (item) {
                                if (item !== null && item.Id !== "" && item.ArticlesNames !== null) {
                                    return item.ArticlesNames.join(', ');
                                }
                                else return "";
                            }
                        },
                        { command: ["edit", "destroy"], title: "&nbsp;" }
                    ],
                editable: "inline"

            });
        var articlesGrid = $("#articlesGrid").kendoGrid(
            {
                scrollable: false,
                dataSource: articlesGridSource,
                toolbar: ["create"],
                schema: articlesModel,
                columns:
                    [
                        { field: "Id", type: "string", hidden: true },
                        { field: "Name", type: "string" },
                        { field: "Date", type: "Date", format: "{0:MM/dd/yyyy}", editor: dateTimeEditor },
                        {
                            field: "JournalsIds", editor: multiSelectEditor, values: journalsInArticlesSource,
                            headerTemplate: setColumnHeader("Journals"),
                            template: function (item) {
                                if (item !== null && item.Id !== "" && item.Journals !== null) {
                                    return item.JournalsNames.join(', ');
                                }
                                else return "";
                            }
                        },
                        {
                            field: "AuthorsIds", editor: multiSelectEditor, values: authorsMainGridSource,
                            headerTemplate: setColumnHeader("Authors"),
                            template: function (item) {
                                if (item !== null && item.Id !== "" && item.AuthorsNames !== null)
                                    return item.AuthorsNames.join(', ');
                                else return "";
                            }
                        },
                        { command: ["edit", "destroy"], title: "&nbsp;" }
                    ],
                editable: "inline"
            });
        var tabStrip = $("#tabStrip").kendoTabStrip({
            dataBound: function () {
            },
            tabPosition: "left",
            animation: {
                open: {
                    effects: "fadeIn"
                }
            }
        });
    }
);
